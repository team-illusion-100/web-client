import {getTestBed, TestBed} from '@angular/core/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {GameService} from '../../src/app/services/game-service/game.service';
import {browser, by, element} from 'protractor';
import {Globals} from '../../src/app/components/globals';

describe('Websockets Scenario', function () {
  //browser.manage().deleteAllCookies();
  //browser.executeScript('window.sessionStorage.clear(); window.localStorage.clear();')
  let injector: TestBed;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [GameService]
    });
    injector = getTestBed();
    //httpMock = injector.get(HttpTestingController);
  });

  it('Websockets Scenario', function () {
    // browser.manage().deleteAllCookies();
    // browser.executeScript('window.sessionStorage.clear(); window.localStorage.clear();');
    // browser.manage().window().setSize(1600, 1000);
    browser.get('https://teamillusion100.surge.sh/');
    browser.sleep(2000);
    let btnLogin = element(by.id('gotoLogin'));
    console.log(btnLogin);
    btnLogin.click();
    browser.sleep(1000);
    let username = element(by.id('usernameInput'));
    let pwd = element(by.id('passwordInput'));
    username.sendKeys('yassine.bouzeya');
    pwd.sendKeys('jwtpass');
    let btnLoginAndToken = element(by.id('signIn'));
    btnLoginAndToken.click();
    browser.sleep(3000);

    browser.get('https://teamillusion100.surge.sh/');
    let startbtn = element(by.id('makeGame'));
    startbtn.click();
    browser.sleep(200);
    let formName = element(by.id('gameName'));
    formName.sendKeys('testName');
    browser.sleep(2000);
    let makebtn = element(by.id('makeLobby'));
    makebtn.click();
    browser.sleep(2000);

    let resultbool;



    expect( testWebsockets()).toBeTruthy();

  });


  function testWebsockets():boolean {
    const globals = TestBed.createComponent(Globals);
    const _this = this;
    let result = false;

    globals.componentInstance.initializeWebSocketConnection(function () {

      browser.sleep(2000);
      globals.componentInstance.stompClient.subscribe('/user/r/makeLobby', function (data) {
        result = true;
      });
      var localStorageMock = (function() {
        var store = {};
        return {
          getItem: function(key) {
            return store[key];
          },
          setItem: function(key, value) {
            store[key] = value.toString();
          },
          clear: function() {
            store = {};
          },
          removeItem: function(key) {
            delete store[key];
          }
        };
      })();
      Object.defineProperty(window, 'localStorage', { value: localStorageMock });
    });




    return result;
  }
});



//this.globals.stompClient.send(
//  '/app/makeLobby',
//  {},
//  JSON.stringify({
//    'gameName': 'e',
//    'turnDuration': 1,
//    'reqBuildings': 2,
//    'maxPlayers': 3,
//    'joinType': 0
//  })
//);


