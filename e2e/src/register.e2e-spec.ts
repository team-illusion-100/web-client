import {browser, by, element} from 'protractor';

describe('Register Scenario', function () {

  it('Register Scenario succes', function () {
    browser.manage().deleteAllCookies();
    browser.manage().window().setSize(1600, 1000);
    browser.get('https://teamillusion100.surge.sh/');
    expect(browser.getTitle()).toEqual('MachiavelliWebClient');
    let btnRegister = element(by.id('gotoRegister'));
    console.log(btnRegister);
    btnRegister.click();
    browser.sleep(1000);
    //VERANDEREN NAAR FORM
    let firstName = element(by.id('voornaam'));
    let lastName = element(by.id('achternaam'));
    let userName = element(by.id('username'));
    let password = element(by.id('password'));
    let email = element(by.id('email'));
    let birthday = element(by.id('birthday'));

    firstName.sendKeys('Abdel');
    lastName.sendKeys('El Makrini');
    userName.sendKeys('e2e23Test');
    password.sendKeys('123456');
    email.sendKeys('e2eeteest@plezant.be');
    birthday.sendKeys('20/02/2002');

    let btnLoginAndToken = element(by.id('registerbtn')); //REGISTER
    btnLoginAndToken.click();
    browser.sleep(3000);
    //REDIRECT HOMEPAGE ADHV TITEL
    expect(browser.getCurrentUrl()).toEqual('https://teamillusion100.surge.sh/login');
  });


  it('Register Scenario username & mail exist', function () {
    browser.manage().deleteAllCookies();
    // browser.executeScript('window.sessionStorage.clear(); window.localStorage.clear();');
    browser.manage().window().setSize(1600, 1000);
    browser.get('https://teamillusion100.surge.sh/');
    expect(browser.getTitle()).toEqual('MachiavelliWebClient');
    let btnRegister = element(by.id('gotoRegister'));
    console.log(btnRegister);
    btnRegister.click();
    browser.sleep(1000);
    //VERANDEREN NAAR FORM
    let firstName = element(by.id('voornaam'));
    let lastName = element(by.id('achternaam'));
    let userName = element(by.id('username'));
    let password = element(by.id('password'));
    let email = element(by.id('email'));
    let birthday = element(by.id('birthday'));

    firstName.sendKeys('Abdel');
    lastName.sendKeys('El Makrini');
    userName.sendKeys('e2eTest');
    password.sendKeys('123456');
    email.sendKeys('e2etest@plezant.be');
    birthday.sendKeys('20/02/2002');

    let btnLoginAndToken = element(by.id('registerbtn')); //REGISTER
    btnLoginAndToken.click();
    browser.sleep(3000);
    //REDIRECT HOMEPAGE ADHV TITEL
    expect(browser.getCurrentUrl()).toEqual('https://teamillusion100.surge.sh/register');
  });


  it('Register Scenario empty form', function () {
    browser.manage().deleteAllCookies();
    // browser.executeScript('window.sessionStorage.clear(); window.localStorage.clear();');
    browser.manage().window().setSize(1600, 1000);
    browser.get('https://teamillusion100.surge.sh/');
    expect(browser.getTitle()).toEqual('MachiavelliWebClient');
    let btnRegister = element(by.id('gotoRegister'));
    console.log(btnRegister);
    btnRegister.click();
    browser.sleep(1000);
    //VERANDEREN NAAR FORM
    let btnLoginAndToken = element(by.id('registerbtn')); //REGISTER
    btnLoginAndToken.click();
    browser.sleep(3000);
    //REDIRECT HOMEPAGE ADHV TITEL
    expect(browser.getCurrentUrl()).toEqual('https://teamillusion100.surge.sh/login');
  });
});
