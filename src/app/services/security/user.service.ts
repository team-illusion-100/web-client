import {Injectable} from '@angular/core';
import {JwtHelper} from 'angular2-jwt';

import {TOKEN_NAME} from './auth.constant';
import {UserDto} from '../../model/UserDto';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({ providedIn: 'root' })
export class UserService {
  //  service that is used to verify if user has a valid JWT token. The service also manages
  // JWT token by adding and removing the token from localStorage.
  // Angular JWT Http service will access the local storage to
  // retrieve the token and pass it to SpringBoot server.

  jwtHelper: JwtHelper = new JwtHelper();
  accessToken: string;
  isAdmin: boolean;

  constructor() {
  }

  login(accessToken: string) {
    const decodedToken = this.jwtHelper.decodeToken(accessToken);

    this.isAdmin = decodedToken.authorities.some(el => el === 'ADMIN_USER');
    this.accessToken = accessToken;
    localStorage.setItem(TOKEN_NAME, accessToken);
    localStorage.setItem('USERNAME', decodedToken.user_name);
  }

  logout() {
    this.accessToken = null;
    this.isAdmin = false;
    localStorage.removeItem(TOKEN_NAME);
  }

  isAdminUser(): boolean {
    return this.isAdmin;
  }

  isUser(): boolean {
    return this.accessToken && !this.isAdmin;
  }

}
