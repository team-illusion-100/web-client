import {Component, OnInit} from '@angular/core';
import {FriendDto} from '../../model/FriendDto';
import {FriendRequestDto} from '../../model/FriendRequestDto';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Globals} from '../globals';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {JsonConvert} from 'json2typescript';
import {Router} from '@angular/router';

@Component({
  selector: 'app-friends',
  templateUrl: './friends.component.html',
  styleUrls: ['./friends.component.css']
})
export class FriendsComponent implements OnInit {
  requestColumnsToDisplay = ['date', 'userName', 'acceptRequest', 'denyRequest'];
  friendColumnsToDisplay = ['status', 'userName', 'gotoFriendProfile', 'removeFriend'];
  sentRequestColumnsToDisplay = ['sentDate', 'receiverUserName', 'removeSentRequest'];
  friends: FriendDto[] = [];
  receivedFriendRequests: FriendRequestDto[] = [];
  enteredUsername;
  addFriendForm;
  friendRequestResponse;
  jsonConvert: JsonConvert = new JsonConvert();
  sentFriendRequests: FriendRequestDto[] = [];

  constructor(private http: HttpClient, private globals: Globals, private router: Router) {
  }

  ngOnInit() {
    this.enteredUsername = new FormControl('', Validators.required);
    this.addFriendForm = new FormGroup({
      enteredUsername: this.enteredUsername
    });
    const _this = this;
    this.http.get<FriendDto[]>(this.globals.backendUrl + '/getFriends').subscribe(data => this.friends = data);
    this.http.get<FriendRequestDto[]>(this.globals.backendUrl + '/getReceivedFriendRequests').subscribe(data => this.receivedFriendRequests = data, null, function () {
      _this.receivedFriendRequests.forEach(function (req) {
        req.parsedTimestamp = req.timestamp.replace('T', ' ');
      });

    });
    this.http.get<FriendRequestDto[]>(this.globals.backendUrl + '/getSentFriendRequests').subscribe(data => this.sentFriendRequests = data, null, function () {
        _this.sentFriendRequests.forEach(function (req) {
          req.parsedTimestamp = req.timestamp.replace('T', ' ');
        });
      }
    );
    this.globals.initializeWebSocketConnection(function () {
      _this.subIncomingfriendRequests();
      _this.subAcceptedFriendRequests();
      _this.subDeniedFriendRequests();
      _this.subRemovedFriendRequests();
      _this.subDeletedAsFriend();
    });
  }

  addFriend() {
    const _this = this;
    if (this.addFriendForm.valid) {
      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        })
      };

      this.http.post<FriendRequestDto>(this.globals.backendUrl + '/sendFriendRequest', this.enteredUsername.value, httpOptions).subscribe(friendRequest => {
          if (friendRequest !== null) {
            _this.friendRequestResponse = 'Vriendschapsverzoek verzonden naar ' + friendRequest.receiver + '.';
            _this.sentFriendRequests.unshift({
              id: friendRequest.id,
              sender: friendRequest.sender,
              receiver: friendRequest.receiver,
              timestamp: friendRequest.timestamp,
              parsedTimestamp: friendRequest.timestamp.replace('T', ' ')
            });
            _this.sentFriendRequests = _this.sentFriendRequests.slice();
          } else {
            document.getElementById('requestResponse').style.display = 'block';
            _this.friendRequestResponse = 'Gebruiker ' + _this.enteredUsername.value + ' niet gevonden of reeds toegevoegd.';
          }
        }
      );
    }
  }

  acceptRequest(friendRequest: FriendRequestDto) {
    const _this = this;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    this.http.post<FriendDto>(this.globals.backendUrl + '/acceptRequest', friendRequest.sender, httpOptions).subscribe(acceptedFriend => {
      _this.friends.unshift({
        id: acceptedFriend.id,
        userName: acceptedFriend.userName,
        email: acceptedFriend.email
      });
      _this.friends = _this.friends.slice();
    });
    this.receivedFriendRequests.splice(this.receivedFriendRequests.findIndex(f => f.sender === friendRequest.sender), 1);
    this.receivedFriendRequests = this.receivedFriendRequests.slice();
  }

  denyRequest(friendRequest: FriendRequestDto) {
    const _this = this;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    this.http.post(this.globals.backendUrl + '/denyRequest', friendRequest.sender, httpOptions).subscribe(function () {
      _this.receivedFriendRequests.splice(_this.receivedFriendRequests.findIndex(f => f.sender === friendRequest.sender), 1);
      _this.receivedFriendRequests = _this.receivedFriendRequests.slice();
    });
  }

  removeRequest(friendRequest: FriendRequestDto) {
    const _this = this;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    this.http.post(this.globals.backendUrl + '/removeRequest', friendRequest.receiver, httpOptions).subscribe(function () {
      _this.sentFriendRequests.splice(_this.sentFriendRequests.findIndex(f => f.receiver === friendRequest.receiver), 1);
      _this.sentFriendRequests = _this.sentFriendRequests.slice();
    });
  }

  gotoFriendProfile(friend: FriendDto) {
    this.router.navigate(['/profile/' + friend.userName]);
  }

  removeFriend(friend: FriendDto) {
    const _this = this;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    this.http.post(this.globals.backendUrl + '/removeFriend', friend, httpOptions).subscribe(function () {
      _this.friends.splice(_this.friends.findIndex(f => f.userName === friend.userName), 1);
      _this.friends = _this.friends.slice();
    });
  }

  // subscribe to incoming friendRequests
  subIncomingfriendRequests() {
    const _this = this;
    _this.globals.stompClient.subscribe('/user/r/friendRequest', function (friendRequest) {
      const jsonObj: object = JSON.parse(friendRequest.body);
      const newRequest = _this.jsonConvert.deserializeObject(jsonObj, FriendRequestDto);
      _this.receivedFriendRequests.unshift({
        id: newRequest.id,
        sender: newRequest.sender,
        receiver: newRequest.receiver,
        timestamp: newRequest.timestamp,
        parsedTimestamp: newRequest.timestamp.replace('T', ' ')
      });
      _this.receivedFriendRequests = _this.receivedFriendRequests.slice();
    }, {id: 'receivedFriendRequests'});
  }

  // subscribe to friends accepting our request
  subAcceptedFriendRequests() {
    const _this = this;
    _this.globals.stompClient.subscribe('/user/r/acceptRequest', function (acceptingFriend) {
      const jsonObj: object = JSON.parse(acceptingFriend.body);
      const friendObj = _this.jsonConvert.deserializeObject(jsonObj, FriendDto);
      _this.friends.unshift({
        id: friendObj.id,
        userName: friendObj.userName,
        email: friendObj.email
      });
      _this.sentFriendRequests.splice(_this.sentFriendRequests.findIndex(f => f.receiver === acceptingFriend.userName), 1);
      _this.sentFriendRequests = _this.sentFriendRequests.slice();
      _this.friends = _this.friends.slice();
    }, {id: 'acceptedRequests'});
  }

  // subscribe to 'friends' denying our request
  subDeniedFriendRequests() {
    const _this = this;
    _this.globals.stompClient.subscribe('/user/r/denyRequest', function (denyingFriend) {
      const jsonObj: object = JSON.parse(denyingFriend.body);
      const friendObj = _this.jsonConvert.deserializeObject(jsonObj, FriendDto);
      _this.sentFriendRequests.splice(_this.sentFriendRequests.findIndex(f => f.receiver === friendObj.userName), 1);
      _this.sentFriendRequests = _this.sentFriendRequests.slice();
    }, {id: 'deniedRequests'});
  }

  // subscribe to 'friends' removing their request
  subRemovedFriendRequests() {
    const _this = this;
    _this.globals.stompClient.subscribe('/user/r/removeRequest', function (removingFriend) {
      const jsonObj: object = JSON.parse(removingFriend.body);
      const friendObj = _this.jsonConvert.deserializeObject(jsonObj, FriendDto);
      _this.receivedFriendRequests.splice(_this.receivedFriendRequests.findIndex(f => f.sender === friendObj.userName), 1);
      _this.receivedFriendRequests = _this.receivedFriendRequests.slice();
    }, {id: 'removedRequests'});
  }

  // subscribe to friends deleting us as their friend
  subDeletedAsFriend() {
    const _this = this;
    _this.globals.stompClient.subscribe('/user/r/deleteFriend', function (deletedFriend) {
      const jsonObj: object = JSON.parse(deletedFriend.body);
      const friendObj = _this.jsonConvert.deserializeObject(jsonObj, FriendDto);
      _this.friends.splice(_this.friends.findIndex(f => f.userName === friendObj.userName), 1);
      _this.friends = _this.friends.slice();
    }, {id: 'removeByFriend'});
  }
}

