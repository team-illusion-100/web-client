import {Injectable} from '@angular/core';
import {HttpEvent, HttpInterceptor, HttpHandler, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class HttpCallsInterceptor implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (!(req.headers.has('Authorization')) && (!req.url.includes('checkusername') && (!req.url.includes('checkemail')) && (!req.url.includes('register') ))) {
      const authReq = req.clone({
        headers: req.headers.set('Authorization', 'bearer ' + localStorage.getItem('access_token'))
      });
      return next.handle(authReq);
    } else {
      return next.handle(req);
    }
  }
}
