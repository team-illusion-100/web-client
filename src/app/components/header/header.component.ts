import {Component, NgModule, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Globals} from '../globals';
import {InviteDto} from '../../model/InviteDto';
import * as $ from 'jquery';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
@NgModule({
  declarations: [
    HeaderComponent
  ],
  exports: [
    HeaderComponent
  ]
})
export class HeaderComponent implements OnInit {
  public isVisible: boolean;
  username: string;
  gamePin: string;

  inviteCount;
  static isHeaderReady=false;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'/*,
      'Authorization': 'my-auth-token'*/
    })
  };

  constructor(private http: HttpClient, private route: ActivatedRoute, private router: Router, private globals: Globals) {
    this.username = localStorage.getItem('USERNAME');
  }

  ngOnInit() {
    this.checkifgameLobby();
    this.isAuthenticated();
  }

  checkifgameLobby() {
    let pageURL: String;
    pageURL = window.location.href;
    let gameXist = pageURL.search('/game/');
    let lobbyXist = pageURL.search('lobby');
    if (gameXist.valueOf() != -1) {
      document.getElementById('gPin').style.display = 'inline';
      this.gamePin = pageURL.substr(pageURL.lastIndexOf('/') + 1);
    } else if (lobbyXist.valueOf() != -1) {
      document.getElementById('gPin').style.display = 'block';
      this.gamePin = pageURL.substr(pageURL.lastIndexOf('/') + 1);
    } else {
      document.getElementById('gPin').style.display = 'none';

    }

  }

  gotoLogin() {
    this.router.navigate(['/login']);
  }

  gotoRegister() {
    this.router.navigate(['/register']);
  }

  gotoHome() {
    this.router.navigate(['/game']);
  }

  gotoProfile() {
    this.router.navigate(['/profile']);
  }

  gotoSettings() {
    this.router.navigate(['/settings']);
  }

  gotoSignOut() {
    localStorage.clear();
    window.location.reload();
    this.gotoHome();
    //  this.http.post<UserDto>('https://machiavelli-talkative-panda.cfapps.io/register', this.httpOptions).subscribe();
  }

  gotoInvites() {
    this.router.navigate(['/invites']);
  }

  isAuthenticated() {
    const accestoken = localStorage.getItem('access_token');
    if (accestoken == null) {
      this.isVisible = true;
    } else {
      this.isVisible = false;
      const _this = this;
      this.http.get<InviteDto[]>(this.globals.backendUrl + '/getInvites').subscribe(data => this.inviteCount = data.length, null, function () {
        if (_this.inviteCount === 0) {
          $('#inviteBadge').text('');
        } else {
          $('#inviteBadge').text(_this.inviteCount);
        }
        _this.globals.initializeWebSocketConnection(function () {
          _this.globals.stompClient.subscribe('/user/r/invite', function () {
            _this.inviteCount += 1;
            $('#inviteBadge').text(_this.inviteCount);
          }, {id: 'invites'});
          HeaderComponent.isHeaderReady=true;
        });
      });
    }
  }
}
