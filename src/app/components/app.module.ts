import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, LOCALE_ID, NgModule} from '@angular/core';
import {MatSidenavModule} from '@angular/material/sidenav';
import {PdfViewerModule} from 'ng2-pdf-viewer';
//HIER AANPASSEN VOOR NEDERLANDS/FRENCH
import {registerLocaleData} from '@angular/common';
import localeFr from '@angular/common/locales/fr';

registerLocaleData(localeFr, 'fr-FR');
import {
  SocialLoginModule,
  AuthServiceConfig,
  GoogleLoginProvider,
  FacebookLoginProvider,
} from 'angular-6-social-login';
import {AppRoutingModule} from './app-routing.module';
import {AboutComponent} from './about/about.component';
import {RulesComponent} from './rules/rules.component';
import {AppComponent} from './app.component';
import {LoginComponent} from './login/login.component';
import {SocialmediaComponent} from './socialmedia/socialmedia.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {Http, HttpModule} from '@angular/http';
import {AuthConfig, AuthHttp} from 'angular2-jwt';
import {TOKEN_NAME} from '../services/security/auth.constant';
import {AuthenticationService} from '../services/security/authentication.service';
import {UserService} from '../services/security/user.service';
import {AdminAuthGuard} from '../guards/admin-auth-guard.service';
import {AuthGuard} from '../guards/auth-guard.service';
import {HeaderComponent} from './header/header.component';
import {GameComponent} from './home/game.component';
import {MenuComponent} from './home/menu/menu.component';
import {RouterModule, Routes} from '@angular/router';
import {GameBoardComponent} from './game/game-board/game-board.component';
import {RivalScreenComponent} from './game/game-board/rival-screen/rival-screen.component';
import {BuildingComponent} from './game/game-board/buildingCard/building.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatCheckboxModule, MatCardModule, MatDialogModule} from '@angular/material';
import {CardsComponent} from './game/game-board/buildingCard/cards.component';
import {CharacterCardComponent} from './game/game-board/character-card/character-card.component';
import {RegisterComponent} from './register/register.component';
import {HttpClientModule} from '@angular/common/http';
import {Globals} from './globals';
import {LobbyComponent} from './lobby/lobby.component';
import {GameService} from '../services/game-service/game.service';
import {HostSettingsComponent} from './host-settings/host-settings.component';
import {SearchGamesComponent} from './search-games/search-games.component';
import {MatInputModule} from '@angular/material';
import {MatSelectModule} from '@angular/material';
import {MatTableModule} from '@angular/material';
import {GameOverviewComponent} from './game-overview/game-overview.component';
import {FriendsComponent} from './friends/friends.component';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {HttpCallsInterceptor} from './HttpCallsInterceptor';
import {GameDockComponent} from './game/game-board/game-dock/game-dock.component';
import {ModalFriendsComponent} from './lobby/modal-friends/modal-friends.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {InvitesComponent} from './invites/invites.component';
import {ChatComponent} from './chat/chat.component';
import {ProfileComponent} from './profile/profile.component';
import {AuthErrorHandler} from './error/AuthErrorHandler';
import {AvatarDialogComponent} from './profile/avatardialog/avatar-dialog.component';
import {ModalCoinCardComponent} from './game/game-board/modal-coin-card/modal-coin-card.component';
import {NotificationboxComponent} from './notificationbox/notificationbox.component';
import {UserSettingsComponent} from './user-settings/user-settings.component';
import {TimerComponent} from './game/timer/timer.component';
import {ModalCharacterPropertyComponent} from './game/game-board/modal-character-property/modal-character-property.component';
import {CenterComponent} from './game/game-board/center/center.component';
import {OtherProfileComponent} from './other-profile/other-profile.component';
import {SearchUsersComponent} from './search-users/search-users.component';
import {GameScoreScreenComponent} from './game-score-screen/game-score-screen.component';


export function authHttpServiceFactory(http: Http) {
  return new AuthHttp(new AuthConfig({
    headerPrefix: 'Bearer',
    tokenName: TOKEN_NAME,
    globalHeaders: [{'Content-Type': 'application/json'}],
    noJwtError: false,
    noTokenScheme: true,
    tokenGetter: (() => localStorage.getItem(TOKEN_NAME))
  }), http);
}


// Configs
export function getAuthServiceConfigs() {
  let config = new AuthServiceConfig(
    [
      {
        id: FacebookLoginProvider.PROVIDER_ID,
        provider: new FacebookLoginProvider('387493368714916')
      },
      {
        id: GoogleLoginProvider.PROVIDER_ID,
        provider: new GoogleLoginProvider('627150552593-3voslbnh1t4rqt468opgomtkkvdkp2ej.apps.googleusercontent.com')

      },
    ]
  );
  return config;
}

const appRoutes: Routes = [
  {path: 'hostSettings', component: HostSettingsComponent},
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'game/:id', component: GameBoardComponent},
  {path: 'gameOverview', component: GameOverviewComponent},
  {path: 'friends', component: FriendsComponent},
  {path: 'invites', component: InvitesComponent},
  {path: 'searchGames', component: SearchGamesComponent},
  {path: 'searchUsers', component: SearchUsersComponent},
  {path: 'social', component: SocialmediaComponent},
  {path: 'about', component: AboutComponent},
  {path: 'rules', component: RulesComponent},
  {path: 'lobby/:id', component: LobbyComponent},
  {path: 'profile', component: ProfileComponent},
  {path: 'profile/:username', component: OtherProfileComponent},
  {path: 'settings', component: UserSettingsComponent},
  {path: 'score/:id', component: GameScoreScreenComponent},
  {path: '**', component: GameComponent},
];

@NgModule({
  declarations: [
    AppComponent,
    AboutComponent,
    LoginComponent,
    HeaderComponent,
    GameComponent,
    MenuComponent,
    HostSettingsComponent,
    GameBoardComponent,
    RivalScreenComponent,
    BuildingComponent,
    CardsComponent,
    CharacterCardComponent,
    SocialmediaComponent,
    RegisterComponent,
    LobbyComponent,
    RulesComponent,
    SearchGamesComponent,
    GameOverviewComponent,
    FriendsComponent,
    GameDockComponent,
    ModalFriendsComponent,
    InvitesComponent,
    ChatComponent,
    ProfileComponent,
    AvatarDialogComponent,
    ModalCoinCardComponent,
    NotificationboxComponent,
    RulesComponent,
    UserSettingsComponent,
    TimerComponent,
    ModalCharacterPropertyComponent,
    CenterComponent,
    OtherProfileComponent,
    SearchUsersComponent,
    GameScoreScreenComponent

  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    SocialLoginModule,
    HttpModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatCheckboxModule,
    PdfViewerModule,
    MatSidenavModule,
    MatCardModule,
    MatDialogModule,
    RouterModule.forRoot(
      appRoutes,

      {enableTracing: false}
    ),
    HttpClientModule,
    MatInputModule,
    MatSelectModule,
    MatTableModule,
    NgbModule
  ],
  providers: [
    {provide: AuthHttp, useFactory: authHttpServiceFactory, deps: [Http]},
    {provide: LOCALE_ID, useValue: 'fr'},
    AuthenticationService,
    UserService,
    AuthGuard,
    AdminAuthGuard,
    Globals,
    GameService,
    {
      provide: AuthServiceConfig,
      useFactory: getAuthServiceConfigs
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpCallsInterceptor,
      multi: true
    },
    {
      provide: ErrorHandler,
      useClass: AuthErrorHandler
    }
  ],
  entryComponents: [
    ModalFriendsComponent,
    AvatarDialogComponent,
    ModalCoinCardComponent,
    ModalCharacterPropertyComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
