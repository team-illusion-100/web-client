import {Component, Input, OnInit} from '@angular/core';
import {CharacterCardDto} from '../../../../model/CharacterCardDto';

@Component({
  selector: 'app-center',
  templateUrl: './center.component.html',
  styleUrls: ['./center.component.css']
})
// The discarded characters in the center
export class CenterComponent implements OnInit {
  @Input() middleFaceUpCharacterCards: CharacterCardDto [] = [];
  @Input() middleFaceDownCharacterCards: CharacterCardDto [] = [];
  @Input() playerOrderNr;

  constructor() {
  }

  ngOnInit() {
  }

}
