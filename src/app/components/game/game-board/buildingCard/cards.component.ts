import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Globals} from '../../../globals';
import {GameService} from '../../../../services/game-service/game.service';

@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.css']
})
export class CardsComponent implements OnInit {
  cards = [];
  constructor(private route: ActivatedRoute, private router: Router, private globals: Globals, private cardService: GameService) { }

  ngOnInit() {
    const _this = this;
    this.globals.initializeWebSocketConnection();
    this.globals.stompClient.subscribe('/r/charactercards/' + this.cardService.getGame().id, function (cardNames) {
      _this.cards = JSON.parse(cardNames.body);
    });
    _this.globals.stompClient.send('/app/charactercards', {}, JSON.stringify({'id': this.cardService.getGame().id}));
  }

}
