import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RivalScreenComponent } from './rival-screen.component';
import {AppModule} from '../../../app.module';

describe('RivalScreenComponent', () => {
  let component: RivalScreenComponent;
  let fixture: ComponentFixture<RivalScreenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[AppModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RivalScreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
