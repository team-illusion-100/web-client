import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalCharacterPropertyComponent } from './modal-character-property.component';
import {AppModule} from '../../../app.module';

describe('ModalCharacterPropertyComponent', () => {
  let component: ModalCharacterPropertyComponent;
  let fixture: ComponentFixture<ModalCharacterPropertyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      //declarations: [ ModalCharacterPropertyComponent ]
      imports: [AppModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalCharacterPropertyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
