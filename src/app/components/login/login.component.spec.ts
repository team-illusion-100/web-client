import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {LoginComponent} from './login.component';
import {AppModule} from '../app.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {By} from '@angular/platform-browser';
import {DebugElement} from '@angular/core';
import {AuthService} from 'angular-6-social-login';
import {AuthenticationService} from '../../services/security/authentication.service';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let authService: AuthenticationService;
  let fixture: ComponentFixture<LoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      //declarations: [ LoginComponent ]
      imports: [AppModule, FormsModule, ReactiveFormsModule] //We add the required ReactiveFormsModule and FormsModule to our test beds imports list
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    authService = TestBed.get(AuthenticationService);
    component = fixture.componentInstance;
    component.ngOnInit(); //We manually trigger the ngOnInit lifecycle function on our component, Angular won’t call this for us.
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('model when empty', () => {
    expect(component.model).toBeTruthy();
  });

  it('calling authService from service when login is called', () => {
    let insertSpy = spyOn(authService, 'login').and.callThrough();
    component.login();
    expect(insertSpy).toHaveBeenCalled();
  });


  it('should work and call the login method right', async(() => {
    let fixture = TestBed.createComponent(LoginComponent);
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      let input = fixture.debugElement.query(By.css('input'));
      let el = input.nativeElement;

      expect(el.value).toBe('peeskillet');

      el.value = 'yassine.bouzeya';
      el.dispatchEvent(new Event('input'));

      expect(fixture.componentInstance.redirectUrl).toBe('someValue');
    });

    it('should correctly render the passed @Input value', () => {
      // there shouldn't be any value initially
      expect(fixture.debugElement.nativeElement.innerHTML).toBe('');

      // let's set the @Input value and then verify again
      component.error = 'Hi there';

      fixture.detectChanges();
      expect(fixture.debugElement.nativeElement.innerHTML).toBe('Hi there');
    });
  }));

  it('calling login from component and see if you get the right response', () => {
    let insertSpy = spyOn(authService, 'login').and.callThrough();
    component.login();

    expect(component.login).toBeTruthy();
    expect(component.login).toBe('true');
  });



  it('should call auth login method', async(() => {
    let loginElement: DebugElement;
    const debugElement = fixture.debugElement;
    let authService = debugElement.injector.get(AuthService);
    let loginSpy = spyOn(authService, 'authState').and.callThrough();
    loginElement = fixture.debugElement.query(By.css('form'));
    // to set values
    //loginElement
    // component.loginForm.controls['username'].setValue('user');
    // component.loginForm.controls['password'].setValue('123');
    loginElement.triggerEventHandler('ngSubmit', null);
    expect(loginSpy).toHaveBeenCalledTimes(1); // check that service is called once
  }));


});



