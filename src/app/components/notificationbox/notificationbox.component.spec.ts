import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotificationboxComponent } from './notificationbox.component';
import {AppModule} from '../app.module';

describe('NotificationboxComponent', () => {
  let component: NotificationboxComponent;
  let fixture: ComponentFixture<NotificationboxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      //declarations: [ NotificationboxComponent ]
      imports: [AppModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotificationboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
