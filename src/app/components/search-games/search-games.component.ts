import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Globals} from '../globals';
import {GameService} from '../../services/game-service/game.service';
import {GameDto} from '../../model/GameDto';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-search-games',
  templateUrl: './search-games.component.html',
  styleUrls: ['./search-games.component.css']
})
export class SearchGamesComponent implements OnInit {
  games: GameDto[] = [];
  columnsToDisplay = ['gameName', 'turnDuration', 'reqBuildings', 'maxPlayers', 'play'];
  isFull = false;
  freshGame: GameDto;

  constructor(private route: ActivatedRoute, private router: Router, private globals: Globals, private gameService: GameService, private http: HttpClient) {}

  ngOnInit() {
    const _this = this;
     this.globals.initializeWebSocketConnection();
    this.http.get<GameDto[]>(this.globals.backendUrl + '/searchGame').subscribe(data => {
      _this.games = data;
      _this.games = _this.games.slice();
    });
  }

  joinLobby(game: GameDto) {
    const _this = this;
    this.freshGame = new GameDto();
    this.http.get<GameDto>(this.globals.backendUrl + '/getLiteGame/' + game.id).subscribe(data => this.freshGame = data, null, function () {
      if (_this.freshGame.nrOfPlayers === _this.freshGame.maxPlayers) {
        _this.isFull = true;
      }
      if (!_this.isFull) {
        _this.globals.stompClient.subscribe('/r/lobby/' + game.id, function () {
          _this.router.navigate(['/lobby', game.id]);
        }, {id: 'lobby' + game.id});
        _this.globals.stompClient.send('/app/joinLobby', {}, JSON.stringify({'id': game.id}));
      }
    });
  }
}
