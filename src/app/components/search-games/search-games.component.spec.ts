import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchGamesComponent } from './search-games.component';
import {AppModule} from '../app.module';

describe('SearchGamesComponent', () => {
  let component: SearchGamesComponent;
  let fixture: ComponentFixture<SearchGamesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      //declarations: [ SearchGamesComponent ]
      imports: [AppModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchGamesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
