import {JsonObject, JsonProperty} from 'json2typescript';

@JsonObject('CharacterDto')
export class CharacterDto {
  @JsonProperty('id', Number)
  id: number = undefined;
  @JsonProperty('characterName', String)
  characterName: String = undefined;
  @JsonProperty('imgPath', String)
  imgPath: String = undefined;
  @JsonProperty('extraGoldColor', Number)
  extraGoldColor: number = undefined;

}
