import {JsonObject, JsonProperty} from 'json2typescript';
import {GameDto} from './GameDto';

@JsonObject('InviteDto')
export class InviteDto {
  @JsonProperty('id', Number)
  id: number = undefined;
  @JsonProperty('invitedUserName', String)
  invitedUserName: String = undefined;
  @JsonProperty('invitingUserName', String)
  invitingUserName: string = undefined;
  @JsonProperty('gameDto', GameDto)
  gameDto: GameDto = undefined;
}
