import {JsonObject, JsonProperty} from 'json2typescript';

@JsonObject('SettingsDto')
export class SettingsDto {
  @JsonProperty('username', String)
  username: String = undefined;
  @JsonProperty('friendRequestNotifications', Boolean)
  friendRequestNotifications: boolean = undefined;
  @JsonProperty('inviteNotifications', Boolean)
  inviteNotifications: boolean = undefined;
  @JsonProperty('otherTurnNotifications', Boolean)
  otherTurnNotifications: boolean = undefined;
  @JsonProperty('myTurnNotifications', Boolean)
  myTurnNotifications: boolean = undefined;
  @JsonProperty('turnIsExpiringNotifications', Boolean)
  turnIsExpiringNotifications: boolean = undefined;
  @JsonProperty('chatNotifications', Boolean)
  chatNotifications: boolean = undefined;
  @JsonProperty('public', Boolean)
  public: boolean = undefined;
}
