import {JsonObject, JsonProperty} from 'json2typescript';
import {BuildingCardDto} from './BuildingCardDto';
import {CharacterCardDto} from './CharacterCardDto';
import {PlayerDto} from './PlayerDto';

@JsonObject('GameDto')
export class GameDto {
  @JsonProperty('id', Number)
  id: number = undefined;
  @JsonProperty('gameName', String)
  gameName: String = undefined;
  @JsonProperty('turnDuration', Number)
  turnDuration: number = undefined;
  @JsonProperty('reqBuildings', Number)
  reqBuildings: number = undefined;
  @JsonProperty('maxPlayers', Number)
  maxPlayers: number = undefined;
  @JsonProperty('nrOfPlayers', Number)
  nrOfPlayers: number = undefined;
  @JsonProperty('joinType', Number)
  joinType: number = undefined;
  @JsonProperty('status', Number)
  status: number = undefined;
  @JsonProperty('buildingCards', [BuildingCardDto])
  buildingCards: BuildingCardDto[] = undefined;
  @JsonProperty('characterCards', [CharacterCardDto])
  characterCards: CharacterCardDto[] = undefined;
  @JsonProperty('players', [CharacterCardDto])
  players: PlayerDto[] = undefined;
  @JsonProperty('playerTurn', Number)
  playerTurn: number = undefined;
  @JsonProperty('coinsLeft', Number)
  coinsLeft: number = undefined;
  @JsonProperty('characterTurn', Number)
  characterTurn: number = undefined;
  @JsonProperty('playersChosen', Boolean)
  playersChosen: boolean = undefined;
  discardedBuildings: BuildingCardDto[] = undefined;
  @JsonProperty('gameOver', Boolean)
  gameOver: boolean = undefined;
}
