import {JsonProperty} from 'json2typescript';
import {CharacterDto} from './CharacterDto';

export class CharacterCardDto {
  @JsonProperty('id', Number)
  id: number = undefined;
  @JsonProperty('character', CharacterDto)
  character: CharacterDto = undefined;
  @JsonProperty('faceUp', Boolean)
  faceUp: boolean = undefined;
  @JsonProperty('killed', Boolean)
  killed: boolean = undefined;
  @JsonProperty('robbed', Boolean)
  robbed: boolean = undefined;
  @JsonProperty('placedMiddle', Boolean)
  placedMiddle: boolean = undefined;
}
// The booleans are not prefixed with 'is' because of deserialization bug, removing prefix seems to be a working fix.
